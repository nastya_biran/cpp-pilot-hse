# С++ курс

Прочитайте [SETUP.md](SETUP.md) и сдайте первую задачу.

Дедлайны по задачам можно наблюдать в https://pilot.cpp-hse.org/.

Стайлгайд описан в [STYLEGUIDE.md](STYLEGUIDE.md).

Правила оценивания в [SCORE.md](SCORE.md).

Записи лекций в [LECTURES.md](LECTURES.md).
